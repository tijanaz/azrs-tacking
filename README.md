# azrs-tracking

Projekat iz Alata za Razvoj Softvera.

Projekti na kojima su primenjivani alati su [battleship-game](https://gitlab.com/NevenaSoldat/battleship-game) i [voice_jump](https://github.com/MATF-RS19/RS010-voice-jump).

# Alati:

1. [Git](https://gitlab.com/tijanaz/azrs-tacking/-/issues/11)
2. [Gammaray](https://gitlab.com/tijanaz/azrs-tacking/-/issues/2)
3. [Git hooks](https://gitlab.com/tijanaz/azrs-tacking/-/issues/1)
4. [ClangTidy](https://gitlab.com/tijanaz/azrs-tacking/-/issues/4)
5. [ClangFormat](https://gitlab.com/tijanaz/azrs-tacking/-/issues/3)
6. [Callgrind](https://gitlab.com/tijanaz/azrs-tacking/-/issues/5)
7. [Valgrind](https://gitlab.com/tijanaz/azrs-tacking/-/issues/7)
8. [CMake](https://gitlab.com/tijanaz/azrs-tacking/-/issues/8)
9. [Generate coverage report](https://gitlab.com/tijanaz/azrs-tacking/-/issues/6)
10. [CI](https://gitlab.com/tijanaz/azrs-tacking/-/issues/9)
11. [Docker](https://gitlab.com/tijanaz/azrs-tacking/-/issues/10)